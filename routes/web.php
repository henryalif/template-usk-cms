<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/blank', function () {
    return view('blank');
});

// Route::prefix('bank')->group(function () {
//     Route::get('/', function () {
//         return view('bank.index');
//     });

//     Route::get('/admin', function () {
//         return view('bank.admin');
//     });
// });

Route::prefix('pages')->group(function () {
    Route::get('/', function () {
        return view('pages.index');
    });

    Route::get('/admin', function () {
        return view('pages.admin');
    });

    Route::get('/approval', function () {
        return view('pages.approval');
    });

    Route::get('/checkout', function () {
        return view('pages.checkout');
    });

    Route::get('/article', function () {
        return view('pages.article');
    });
});

// Route::get('/history', function () {
//     return view('history.index');
// });

Route::get('/user', function () {
    return view('user');
});
