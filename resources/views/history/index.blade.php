@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">History</h3>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="table table-striped table-responsive-xl" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Item</th>
                                    <th>Nominal</th>
                                    <th>Qty</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td>1</td>
                                    <td>19/01/2022</td>
                                    <td><span class="badge badge-danger">TOP UP</div></td>
                                    {{-- <td>Top Up</td> --}}
                                    <td>E-Money</td>
                                    <td>Rp. 25.000</td>
                                    <td>1</td>
                                    {{-- <td>Completed</td> --}}
                                    <td><span class="badge badge-warning">PENDING</div></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>19/01/2022</td>
                                    <td><span class="badge badge-info">PURCHASE</div></td>
                                    {{-- <td>Top Up</td> --}}
                                    <td>E-Money</td>
                                    <td>Rp. 25.000</td>
                                    <td>1</td>
                                    {{-- <td>Completed</td> --}}
                                    <td><span class="badge badge-success">Completed</div></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>

@endsection
