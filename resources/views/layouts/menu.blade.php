<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/home">
        <i class=" fas fa-home"></i><span>Dashboard</span>
    </a>
</li>

{{-- <li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/pages">
        <i class=" fas fa-utensils"></i><span>pages</span>
    </a>
</li> --}}
{{--
<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/bank">
        <i class=" fas fa-building"></i><span>Bank</span>
    </a>
</li> --}}

{{-- <li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/history">
        <i class=" fas fa-history"></i><span>History</span>
    </a>
</li> --}}

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/pages/admin">
        <i class=" fas fa-truck-loading"></i><span>Add Pages</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/pages/article">
        <i class=" fas fa-truck-loading"></i><span>Add Article</span>
    </a>
</li>

{{-- <li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/bank/admin">
        <i class=" fas fa-users-cog"></i><span>Bank Admin</span>
    </a>
</li> --}}

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/pages/approval">
        <i class=" fas fa-store"></i><span>Comments Approval</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/user">
        <i class=" fas fa-users"></i><span>Add User</span>
    </a>
</li>
