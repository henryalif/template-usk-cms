@extends('layouts.app')


@section('content')
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Detail Comments : </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h6>" The comments "</h6>
            </div>
            {{-- <div class="modal-footer">
            <button type="readonly" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
            </div> --}}
        </div>
        </div>
    </div>
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Comments Approval</h3>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="table table-striped" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Article</th>
                                    <th>Comments</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td>1</td>
                                    <td><span class="badge badge-dark">Henry</div></td>
                                    <td><span class="badge badge-info"><strong>Article 1</strong></span></td>
                                    <td>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            Detail
                                        </button>
                                    </td>
                                    <td>
                                        <a class="btn btn-success btn-md" href="#" role="button"><i class="fas fa-check-square"></i></a>
                                        <a class="btn btn-danger btn-md" href="#" role="button"><i class="fas fa-times-circle"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>

@endsection
