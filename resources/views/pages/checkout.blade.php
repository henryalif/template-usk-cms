@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h5>Balance : <strong>Rp. 50.000</strong></h5>
        </div>
        <div class="section-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        {{-- <div class="card-header">Checkout {{ count($carts) > 0 ? "#" . $carts[0]->invoice_id : "" }}</div> --}}
                        <div class="card-body" style="height: auto; overflow: auto">
                            <table id="example" class="table table-striped " style="width:100%">
                                <thead class="text-center">
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th><strong>Total</strong></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                        <tr>
                                            <td>1</td>
                                            <td>Bakso</td>
                                            <td><span class="badge badge-dark">1</span></td>
                                            <td>Rp. 90.000</td>
                                            <td><span class="badge badge-primary"><strong>Rp. 90.000</strong></span></td>
                                        </tr>
                                </tbody>
                            </table>
                            <br>
                            <button type="button" class="btn btn-block btn-md btn-outline-primary" disabled> Total Transaction : <strong>Rp. 90.000 </strong></button>
                            <a href="#" class="btn btn-block btn-md btn-success">Pay!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>

@endsection
