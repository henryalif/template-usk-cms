@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Product Page</h3>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header mb-0">
                                    <h5 class="card-title">Product Name</h5>
                                </div>
                                <div class="card-body">
                                    <p class="card-text mb-0">Product description.</p>
                                    <h6 class="card-text mb-3">Rp. 8.000</h6>
                                    {{-- <form method="POST" action="#">
                                        <input type="number" name="jumlah" class="form-control" value="1">
                                        <input type="hidden" name="barang_id" value="#">
                                        <button class="btn btn-primary" type="submit"><i class="fas fa-plus-circle"></i></button>
                                    </form> --}}

                                    <div class="input-group mb-3">
                                        <input type="number" class="form-control" name="qty" placeholder="Item" aria-label="Item" aria-describedby="button-addon2" value="1">
                                        <input type="hidden" name="barang_id" value="#">
                                        <button class="btn btn-outline-success" type="submit" id="button-addon2"><i class="fas fa-cart-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body" style="height: auto; overflow: auto">
                            <table class="table table-bordered table-hovered table-striped table-responsive table-scroll" style="height: auto">
                                <thead class="text-center">
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th><strong>Total</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Name</td>
                                        <td>1</td>
                                        <td>Rp. 59.000</td>
                                        <td><strong>Rp. 59.000</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <p>Total Transaction :</p>
                            <button type="button" class="btn btn-block btn-md btn-outline-primary" disabled><strong>Rp. 70.000</strong></button>
                            <button type="button" class="btn btn-block btn-md btn-success">Checkout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
    $('#example').DataTable({
    "scrollY": "50vh",
    "scrollCollapse": true,
    });
    } );
</script>

@endsection
