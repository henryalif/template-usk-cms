@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Dashboard</h3>
        </div>

        <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <a href="#!" class="card-icon bg-info">
                        <i class="fas fa-file-invoice-dollar"></i>
                    </a>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Article</h4>
                        </div>
                        <div class="card-body">Tester</div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-money-check-alt"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Page</h4>
                        </div>
                        <div class="card-body">Tester</div>
                    </div>
                </div>
            </div>
{{--
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-search-dollar"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Product</h4>
                        </div>
                        <div class="card-body">Tester</div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-clipboard-check"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Transaction</h4>
                        </div>
                        <div class="card-body">Tester</div>
                    </div>
                </div>
            </div>
        </div> --}}

        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-users"></i>
                </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>User</h4>
                </div>
                <div class="card-body">Tester</div>
                </div>
            </div>
        </div>
    </section>
@endsection

